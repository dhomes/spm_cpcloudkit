// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

// https://dhomes@bitbucket.org/dhomes/spm_cpcloudkit.git
import PackageDescription

let major=1
let minor=0
let point=10006

let package = Package(
    name: "CPCloudKit",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "CPCloudKit",
            targets: ["CPCloudKit"]),
    ],
    dependencies: [
        .package(url: "https://bitbucket.org/dhomes/spm_cpcorekit.git", branch: "master")
    ],
    targets: [
        .binaryTarget(
            name: "CPCloudKit",
            url: "https://bitbucket.org/dhomes/spm_cpcloudkit/downloads/CPCloudKit.xcframework.zip",
            checksum: "a8dbe885ac7a737ad1dc01848bb0413620a06620b58902761fa1ddfbdf3c3961"
        )
    ]
)
